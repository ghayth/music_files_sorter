package com.ghayth.musicsorter.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class DirectoryUtilsTest {

	@Test
	public void testAddSeparatorAtPathEndIfNecessary() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateDirectoryIfNecessary() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllFilesPaths() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateUriBasedOnYear() {
		String result = DirectoryUtils.createUriBasedOnYear("1985");
		assertEquals("1980s\\1985\\", result);

		result = DirectoryUtils.createUriBasedOnYear("2000");
		assertEquals("2000s\\2000\\", result);

		result = DirectoryUtils.createUriBasedOnYear("198");
		assertEquals("198\\", result);

		result = DirectoryUtils.createUriBasedOnYear(null);
		assertEquals("", result);
	}

}
