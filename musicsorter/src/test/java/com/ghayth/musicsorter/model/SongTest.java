//package com.ghayth.musicsorter.model;
//
//import static org.junit.Assert.*;
//
//import org.junit.Test;
//
///**
// * @author Ghayth AYARI
// *
// */
//public class SongTest {
//
//	/**
//	 * Test method for {@link com.ghayth.musicsorter.model.Song#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsObject() {
//		
//		Song song1 = new Song("Maroon 5", "This Love", "2005", "C:\\");
//		Song song2 = new Song("Maroon 5", "This Love", "2007", "C:\\");
//		Song song3 = new Song("Maroon 5", "This Love", "2007", "C:\\test");
//		Song song4 = new Song("Maroon 5", "Animals", "2005", "C:\\");
//		Song song5 = new Song("Maroon5", "This Love", "2005", "C:\\");
//		Song song6 = new Song("Synapson", "This Love", "2005", "C:\\");
//		
//		assertEquals(song1, song2);
//		assertEquals(song1, song3);
//		assertNotEquals(song1, song4);
//		assertNotEquals(song1, song5);
//		assertNotEquals(song1, song6);
//	}
//
//}
