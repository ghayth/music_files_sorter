///**
// * 
// */
//package com.ghayth.musicsorter.helper.helper;
//
//import static org.junit.Assert.*;
//
//import java.io.UnsupportedEncodingException;
//
//import org.junit.Test;
//
///**
// * @author Ghayth AYARI
// *
// */
//public class GeneralHelperTest {
//
//	/**
//	 * Test method for {@link com.ghayth.musicsorter.helper.helper.GeneralHelper#encodeParameterForURL(java.lang.String)}.
//	 */
//	@Test
//	public void testEncodeParameterForURL() {
//		
//		String stringToEncode = "All In You";
//		String expectedEncodedString = "all+in+you";
//		
//		try {
//			assertEquals(expectedEncodedString, GeneralHelper.encodeParameterForURL(stringToEncode));
//			
//		} catch (UnsupportedEncodingException e) {
//			fail("Method is not supposed to throw an exception!");
//		}
//	}
//
//	
//	/**
//	 * Test method for {@link com.ghayth.musicsorter.helper.helper.GeneralHelper#formatExtension(java.lang.String)}.
//	 */
//	@Test
//	public void testFormatExtension() {
//		
//		String extensionToFormat1 = "  .mp3 ";
//		String extensionToFormat2 = "mp3 ";
//		String expectedFormattedExtension = ".mp3";
//		
//		assertEquals(expectedFormattedExtension, GeneralHelper.formatExtension(extensionToFormat1));
//		assertEquals(expectedFormattedExtension, GeneralHelper.formatExtension(extensionToFormat2));
//	}
//
//}
