package com.ghayth.musicsorter.exception;

/**
 * @author Ghayth AYARI
 *
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = -8798653292714197071L;

	private BusinessErrorEnum functionalErrorEnum;

	/**
	 * @param functionalErrorEnum
	 */
	public BusinessException(BusinessErrorEnum functionalErrorEnum) {
		super();
		this.functionalErrorEnum = functionalErrorEnum;
	}

	/**
	 * @param message
	 */
	public BusinessException(String message, BusinessErrorEnum functionalErrorEnum) {
		super(message);
		this.functionalErrorEnum = functionalErrorEnum;
	}

	/**
	 * @param cause
	 */
	public BusinessException(Throwable cause, BusinessErrorEnum functionalErrorEnum) {
		super(cause);
		this.functionalErrorEnum = functionalErrorEnum;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public BusinessException(String message, Throwable cause, BusinessErrorEnum functionalErrorEnum) {
		super(message, cause);
		this.functionalErrorEnum = functionalErrorEnum;
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			BusinessErrorEnum functionalErrorEnum) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.functionalErrorEnum = functionalErrorEnum;
	}

	/**
	 * @return the functionalErrorEnum
	 */
	public BusinessErrorEnum getFunctionalErrorEnum() {
		return functionalErrorEnum;
	}

}
