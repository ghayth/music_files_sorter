package com.ghayth.musicsorter.exception;

/**
 * @author Ghayth AYARI
 *
 */
public class FileManagementException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6481559840281325614L;

	/**
	 * @param message
	 */
	public FileManagementException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public FileManagementException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FileManagementException(String message, Throwable cause) {
		super(message, cause);
	}

}
