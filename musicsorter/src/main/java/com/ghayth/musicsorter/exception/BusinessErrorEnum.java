package com.ghayth.musicsorter.exception;

public enum BusinessErrorEnum {
	E01("Unable to find track on Spotify API"),
	E02("Missing artist and/or title in song's metadata!");

	private final String value;

	BusinessErrorEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
