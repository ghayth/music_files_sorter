package com.ghayth.musicsorter.exception;

/**
 * @author Ghayth AYARI
 *
 */
public class TechnicalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6481559840281325614L;

	/**
	 * @param message
	 */
	public TechnicalException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public TechnicalException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public TechnicalException(String message, Throwable cause) {
		super(message, cause);
	}

}
