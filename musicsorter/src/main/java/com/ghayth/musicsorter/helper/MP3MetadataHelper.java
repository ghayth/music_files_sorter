package com.ghayth.musicsorter.helper;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.model.Song;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v1Tag;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.ID3v22Tag;
import com.mpatric.mp3agic.Mp3File;

/**
 * This class does operations on MP3 files metadata
 * 
 * @author Ghayth AYARI
 *
 */
public class MP3MetadataHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(MP3MetadataHelper.class);

	/**
	 * Removes custom tags, and unneeded ID3V1 and ID3V2 tags.
	 * 
	 * @param mp3File
	 */
	public static void cleanMP3FileMetadata(Mp3File mp3File) {
		mp3File.removeCustomTag();

		if (mp3File.hasId3v1Tag()) {
			mp3File.getId3v1Tag().setComment(null);
		}

		if (mp3File.hasId3v2Tag()) {
			ID3v2 id3v2 = mp3File.getId3v2Tag();

			id3v2.clearAlbumImage();
			id3v2.setComment(null);
			id3v2.setCommercialUrl(null);
			id3v2.setCopyright(null);
			id3v2.setCopyrightUrl(null);
			id3v2.setItunesComment(null);
			id3v2.setKey(null);
			id3v2.setPaymentUrl(null);
			id3v2.setPartOfSet(null);
		}
	}

	public static final void setSongMP3Metadata(Song song) {

		if (song != null && song.getMp3File() != null) {
			cleanMP3FileMetadata(song.getMp3File());
			setSongID3V1(song);
			setSongID3V2(song);
		}
	}

	protected static void setSongID3V1(Song song) {

		// Initialize ID3V1 metadata if needed to
		if (!song.getMp3File().hasId3v1Tag()) {
			song.getMp3File().setId3v1Tag(new ID3v1Tag());
		}

		ID3v1 id3v1 = song.getMp3File().getId3v1Tag();

		// Set fields
		id3v1.setAlbum(song.getAlbum());
		id3v1.setArtist(song.getArtist());
		id3v1.setTitle(song.getTitle());
		id3v1.setTrack(song.getTrackNumber());
		id3v1.setYear(song.getYear());
	}

	protected static void setSongID3V2(Song song) {

		// Initialize ID3V2 metadata if needed to
		if (!song.getMp3File().hasId3v2Tag()) {
			song.getMp3File().setId3v2Tag(new ID3v22Tag());
		}

		ID3v2 id3v2 = song.getMp3File().getId3v2Tag();

		// Set fields
		id3v2.setAlbum(song.getAlbum());
		id3v2.setArtist(song.getArtist());
		id3v2.setTitle(song.getTitle());
		id3v2.setTrack(song.getTrackNumber());
		id3v2.setYear(song.getYear());
		id3v2.setUrl(song.getUrl());
		id3v2.setAlbumArtist(song.getAlbumArtist());

		// Set album image
		try {
			byte[] imageBytes = GeneralHelper.extractImageBytes(song.getImageURL());
			id3v2.setAlbumImage(imageBytes, "image/jpeg");

		} catch (IOException e) {
			LOGGER.error("Unable to read image bytes for " + song, e);
		}
	}

}
