package com.ghayth.musicsorter.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.ghayth.musicsorter.model.Song;

/**
 * 
 * @author Ghayth AYARI
 *
 */
public class GeneralHelper {

	/**
	 * Extracts bytes of the image related to the provided URL.
	 * 
	 * @param imageUrl
	 * 
	 * @return
	 * 
	 * @throws IOException
	 */
	public static byte[] extractImageBytes(String imageUrl) throws IOException {

		URL url = new URL(imageUrl);
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent", "Firefox");

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try (InputStream inputStream = conn.getInputStream()) {
			int n = 0;
			byte[] buffer = new byte[1024];
			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}
		}

		return output.toByteArray();
	}

	/**
	 * Generates file name from song information.
	 * 
	 * @param song
	 * 
	 * @return
	 */
	public static String generateFileNameFromSong(Song song) {

		String finalSongName = cleanStringFromSpecialCharacters(song.getArtist());

		if (StringUtils.isNotEmpty(song.getTitle())) {
			finalSongName += " - " + cleanStringFromSpecialCharacters(song.getTitle());
		}

		return finalSongName;
	}

	public static String cleanStringFromSpecialCharacters(String stringToClean) {

		String result = null;

		if (stringToClean != null) {
			result = StringUtils.trim(StringUtils.replaceEach(stringToClean,
					new String[] { "*", "/", ":", "\\", "?", "\"", "<", ">", "|" },
					new String[] { " ", " ", " ", " ", " ", " ", " ", " ", " " }));
		}

		return result;
	}

	public static String generateSearchQueryFromSongArtistAndTitle(Song song) {

		String result = "";

		String artist = song.getArtist();
		if (StringUtils.isNotEmpty(artist)) {
			result = cleanSongArtistOrTitle(cleanStringFromSpecialCharacters(artist));
		}

		String title = song.getTitle();
		if (StringUtils.isNotEmpty(title)) {
			result += " " + cleanSongArtistOrTitle(cleanStringFromSpecialCharacters(title));
		}

		return result;
	}

	public static String cleanSongArtistOrTitle(String stringToClean) {

		String result = "";

		if (StringUtils.isNotBlank(stringToClean)) {
			result = stringToClean.toLowerCase().replace("_", " ").replace("-", " ").replace("&", " ")
					.replaceAll("[w][w][w]\\.[a-z0-9\\.-]{1,}", "").replaceAll("\\((.){0,}\\)", "")
					.replaceAll("[a-z0-9-\\.]{0,}\\.[c][o][m]", "").replaceAll("\\[.*\\]", "")
					.replaceAll("[a-z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-z0-9.-]+", "").replaceAll("feat", "")
					.replaceAll("featuring", "").replaceAll("ft\\.", "").replaceAll(".", "");
		}

		return result;
	}

	public static String generateSearchQueryFromSongFileName(Song song) {

		String fileName = FilenameUtils.getBaseName(song.getPath());

		return cleanSongArtistOrTitle(fileName);
	}

}
