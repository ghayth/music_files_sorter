package com.ghayth.musicsorter.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang3.StringUtils;

import com.ghayth.musicsorter.worker.DotsNumberToDisplayCalculatingWorker;
import com.ghayth.musicsorter.worker.MusicSorterWorker;

/**
 * @author Ghayth AYARI
 *
 */
public class MainWindow extends JFrame implements ActionListener {

	private static final long serialVersionUID = -2602253219355947158L;

	private static final String CMD_EXIT = "Exit";
	private static final String CMD_BROWSE = "Browse";
	private static final String CMD_GO = "Go!";
	private static final String CMD_STOP = "◼";
	private static final String CMD_DEST_IS_SAME = "Destination directory is the same as the source";
	private static final String CMD_COMPLETE_MISSING_METADATA = "Complete missing metadata";

	private static final String MSG_SURE_TO_EXIT = "Are you sure you want to exit? Your unsaved work will be lost.";

	private static final String WINDOW_TITLE_BROWSE_DIRECTORY = "Browse directory";
	private static final String WINDOW_TITLE_EXIT = "Exit";
	private static final String WINDOW_TITLE_MAIN = "Music Sorter V1 by Ghayth AYARI";

	private static final String LABEL_WAITING_FOR_USER_ACTION = "Waiting for user action...";
	private static final String LABEL_PROCESSING = "Processing";

	private static final int PROGRESS_BAR_MINIMUM = 0;

	private JFileChooser fcInputDirectory;
	private JFileChooser fcDestinationDirectory;

	private JTextField tfSource;
	private JTextField tfDestination;

	private JButton bBrowseSource;
	private JButton bBrowseDest;

	private JCheckBox cbSameAsSourceDirectory;
	private JCheckBox cbCompleteMissingMetadata;

	private JButton bGo;
	private JButton bStop;

	private JLabel lStatus = new JLabel(LABEL_WAITING_FOR_USER_ACTION);

	private JProgressBar pbar;
	private JTextArea textArea;

	private DotsNumberToDisplayCalculatingWorker dotsNumberToDisplayCalculatingWorker;
	private MusicSorterWorker musicSorterWorker;

	public static void main(String[] args) {

		// Launch the main window
		new MainWindow();
	}

	public MainWindow() {

		// When "X" is clicked
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				processExitAction();
			}
		});

		buildFrame();

		// Position window
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		setMinimumSize(getSize());
		setTitle(WINDOW_TITLE_MAIN);
		setVisible(true);
	}

	private void buildFrame() {

		// Initialize global panel
		JPanel panels = new JPanel();
		panels.setLayout(new GridLayout(7, 1, 0, 0));

		// Choosing source directory panel
		// *******************************************************************
		JPanel panelSource = new JPanel();
		panelSource.setBorder(BorderFactory.createTitledBorder("1. Source directory"));
		panelSource.setLayout(new BoxLayout(panelSource, BoxLayout.X_AXIS));

		// Browse Button
		bBrowseSource = new JButton(CMD_BROWSE);
		bBrowseSource.addActionListener(this);
		Dimension bBrowseSourceDimension = bBrowseSource.getPreferredSize();
		bBrowseSource.setSize(bBrowseSourceDimension);
		bBrowseSource.setMaximumSize(bBrowseSourceDimension);
		bBrowseSource.setHorizontalTextPosition(JButton.CENTER);
		panelSource.add(bBrowseSource);

		// Space between button and text field
		JLabel labelSpace = new JLabel();
		labelSpace.setPreferredSize(new Dimension(20, 5));
		labelSpace.setMaximumSize(new Dimension(20, 5));
		labelSpace.setMinimumSize(new Dimension(20, 5));
		panelSource.add(labelSpace);

		// Chosen directory text field
		tfSource = new JTextField(80);
		Dimension tfSourceDimension = tfSource.getPreferredSize();
		tfSource.setSize(tfSourceDimension.width, bBrowseSourceDimension.height);
		tfSource.setMaximumSize(tfSource.getSize());
		tfSource.getDocument().addDocumentListener(new MyDocumentListener());

		tfSource.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}
		});

		panelSource.add(tfSource);

		// Choosing directory dialogue
		fcInputDirectory = new JFileChooser();
		fcInputDirectory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fcInputDirectory.setDialogTitle(WINDOW_TITLE_BROWSE_DIRECTORY);

		// Choosing directory row
		JPanel rowSource = new JPanel();
		rowSource.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		rowSource.add(panelSource);
		panels.add(rowSource);

		// Dimension panelSourceDimension = panelSource.getPreferredSize();

		// Choosing destination directory panel
		// *******************************************************************
		JPanel panelDest = new JPanel();
		panelDest.setBorder(BorderFactory.createTitledBorder("2. Destination Directory"));
		panelDest.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel panelBrowseDest = new JPanel();
		panelBrowseDest.setLayout(new BoxLayout(panelBrowseDest, BoxLayout.X_AXIS));

		// Check box
		cbSameAsSourceDirectory = new JCheckBox(CMD_DEST_IS_SAME);
		cbSameAsSourceDirectory.setSelected(true);
		cbSameAsSourceDirectory.addActionListener(this);
		panelDest.add(cbSameAsSourceDirectory);

		// Browse Button
		bBrowseDest = new JButton(CMD_BROWSE);
		bBrowseDest.addActionListener(this);
		Dimension bBrowseDestDimension = bBrowseDest.getPreferredSize();
		bBrowseDest.setSize(bBrowseDestDimension);
		bBrowseDest.setHorizontalTextPosition(JButton.CENTER);
		bBrowseDest.setEnabled(false);
		panelBrowseDest.add(bBrowseDest);

		// Space between button and text field
		JLabel labelSpace2 = new JLabel();
		labelSpace2.setPreferredSize(new Dimension(20, 5));
		labelSpace2.setMaximumSize(new Dimension(20, 5));
		labelSpace2.setMinimumSize(new Dimension(20, 5));
		panelBrowseDest.add(labelSpace2);

		// Chosen directory text field
		tfDestination = new JTextField(80);
		Dimension tfDestDimension = tfDestination.getPreferredSize();
		tfDestination.setSize(tfDestDimension.width, bBrowseDestDimension.height);
		tfDestination.setMaximumSize(tfDestination.getSize());
		tfDestination.setEnabled(false);

		tfDestination.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				checkSourceAndDestinationDirectories();
			}
		});

		panelBrowseDest.add(tfDestination);
		panelDest.add(panelBrowseDest);

		// Choosing directory dialogue
		fcDestinationDirectory = new JFileChooser();
		fcDestinationDirectory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fcDestinationDirectory.setDialogTitle(WINDOW_TITLE_BROWSE_DIRECTORY);

		// Choosing directory row
		JPanel rowDest = new JPanel();
		rowDest.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
		rowDest.add(panelDest);
		panels.add(rowDest);

		Dimension panelDestDimension = panelDest.getPreferredSize();

		// Processing options
		// *******************************************************************
		JPanel panelYears = new JPanel();
		panelYears.setBorder(BorderFactory.createTitledBorder("3. Processing options"));
		panelYears.setLayout(new BoxLayout(panelYears, BoxLayout.Y_AXIS));

		// Check boxes
		cbCompleteMissingMetadata = new JCheckBox(CMD_COMPLETE_MISSING_METADATA);
		cbCompleteMissingMetadata.addActionListener(this);
		cbCompleteMissingMetadata.setSelected(true);
		cbCompleteMissingMetadata.setEnabled(false);
		panelYears.add(cbCompleteMissingMetadata);

		Dimension panelYearsDimension = panelYears.getPreferredSize();
		int panelYearsWidth = (panelDestDimension.width > panelYearsDimension.width) ? panelDestDimension.width
				: panelYearsDimension.width;
		panelYears.setPreferredSize(new Dimension(panelYearsWidth, panelYearsDimension.getSize().height));

		JPanel rowYears = new JPanel();
		rowYears.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 40));
		rowYears.add(panelYears);

		panels.add(rowYears);

		// Action buttons panel
		// *******************************************************************
		JPanel panelActions = new JPanel();
		panelActions.setLayout(new BoxLayout(panelActions, BoxLayout.X_AXIS));
		panelActions.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 40));

		// Start button
		bGo = new JButton(CMD_GO);
		bGo.setFont(new Font(bGo.getFont().getName(), Font.BOLD, bGo.getFont().getSize() + 5));
		bGo.addActionListener(this);
		bGo.setEnabled(false);

		JLabel labelSpace3 = new JLabel();
		labelSpace3.setPreferredSize(new Dimension(20, 5));
		labelSpace3.setMaximumSize(new Dimension(20, 5));
		labelSpace3.setMinimumSize(new Dimension(20, 5));

		bStop = new JButton(CMD_STOP);
		bStop.setFont(new Font(bStop.getFont().getName(), Font.BOLD, bStop.getFont().getSize()));
		bStop.setEnabled(false);
		bStop.addActionListener(this);

		panelActions.add(bGo);
		panelActions.add(labelSpace3);
		panelActions.add(bStop);
		panels.add(panelActions);

		// label panel
		// *******************************************************************
		JPanel panelLabel = new JPanel();
		panelLabel.setLayout(new BoxLayout(panelLabel, BoxLayout.X_AXIS));
		panelLabel.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 40));
		panelLabel.add(lStatus);
		panels.add(panelLabel);

		// Add a progress bar and a general status label
		// *******************************************************************
		JPanel panelProgressBar = new JPanel();
		panelProgressBar.setLayout(new BoxLayout(panelProgressBar, BoxLayout.X_AXIS));
		panelProgressBar.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 40));

		pbar = new JProgressBar();
		pbar.setMinimum(PROGRESS_BAR_MINIMUM);
		pbar.setStringPainted(true);
		panelProgressBar.add(pbar);
		panels.add(panelProgressBar);

		// Add a text area for logs
		// *******************************************************************
		JPanel panelTextArea = new JPanel();
		panelTextArea.setLayout(new BoxLayout(panelTextArea, BoxLayout.X_AXIS));
		panelTextArea.setBorder(BorderFactory.createEmptyBorder(0, 40, 30, 40));

		textArea = new JTextArea();
		textArea.setColumns(80);
		textArea.setLineWrap(true);
		textArea.setRows(10);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		textArea.setForeground(Color.RED);
		textArea.setMinimumSize(new Dimension(300, 300));
		JScrollPane jScrollPane = new JScrollPane(textArea);
		panelTextArea.add(jScrollPane);

		panels.add(panelTextArea);

		// Add rows to the current window
		// *******************************************************************
		add(panels);
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String selection = e.getActionCommand();

		if (selection.equals(CMD_EXIT)) {
			processExitAction();
		} else if (selection.equals(CMD_BROWSE)) {
			processBrowseAction(e);
		} else if (selection.equals(CMD_DEST_IS_SAME)) {
			sameInputAndOutputFolderSelected();
		} else if (selection.equals(CMD_GO)) {
			processMusicFiles();
		} else if (selection.equals(CMD_STOP)) {
			processStopAction();
		}
	}

	private void sameInputAndOutputFolderSelected() {

		if (cbSameAsSourceDirectory.isSelected()) {
			bBrowseDest.setEnabled(false);
			tfDestination.setEnabled(false);
			tfDestination.setText(tfSource.getText());
		} else {
			bBrowseDest.setEnabled(true);
			tfDestination.setEnabled(true);
		}
	}

	private void processBrowseAction(ActionEvent e) {

		if (e.getSource().equals(bBrowseSource)) {
			int returnState = fcInputDirectory.showOpenDialog(this);
			if (returnState == JFileChooser.APPROVE_OPTION) {
				String selectedDirectoryPath = fcInputDirectory.getSelectedFile().getPath();
				tfSource.setText(selectedDirectoryPath);

				if (cbSameAsSourceDirectory.isSelected())
					tfDestination.setText(selectedDirectoryPath);
			}
		} else {
			int returnState = fcDestinationDirectory.showOpenDialog(this);
			if (returnState == JFileChooser.APPROVE_OPTION) {
				String selectedDirectoryPath = fcDestinationDirectory.getSelectedFile().getPath();
				tfDestination.setText(selectedDirectoryPath);
			}
		}
	}

	private void processExitAction() {

		int choice = JOptionPane.showConfirmDialog(this, MSG_SURE_TO_EXIT, WINDOW_TITLE_EXIT, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);

		if (choice == 0) // YES
			System.exit(0);
		else if (choice == 1) // NO
		{
		}
	}

	private void processStopAction() {
		// Disable/enable elements
		bGo.setEnabled(true);
		bStop.setEnabled(false);
		bBrowseSource.setEnabled(true);
		bBrowseDest.setEnabled(true);
		tfSource.setEnabled(true);
		tfDestination.setEnabled(true);
		pbar.setEnabled(false);
		pbar.setIndeterminate(false);
		sameInputAndOutputFolderSelected();

		// Set label
		lStatus.setText(LABEL_WAITING_FOR_USER_ACTION);

		// Stop the dots in the "Processing" label
		if (dotsNumberToDisplayCalculatingWorker != null) {
			dotsNumberToDisplayCalculatingWorker.cancel(true);
		}

		// Stop the music sorter worker
		if (musicSorterWorker != null) {
			musicSorterWorker.cancel(true);
		}
	}

	private void processMusicFiles() {

		// Disable/enable elements
		bGo.setEnabled(false);
		bStop.setEnabled(true);
		bBrowseSource.setEnabled(false);
		bBrowseDest.setEnabled(false);
		tfSource.setEnabled(false);
		tfSource.setBackground(null);
		tfDestination.setEnabled(false);
		tfDestination.setBackground(null);
		lStatus.setForeground(null);
		pbar.setEnabled(true);
		pbar.setValue(0);
		pbar.setIndeterminate(true);

		// Input directory
		String chosenInputDirectoryPath = tfSource.getText();

		if (StringUtils.isNotEmpty(chosenInputDirectoryPath)) {

			File chosenInputDirectory = new File(chosenInputDirectoryPath);
			if (chosenInputDirectory.isDirectory()) {
				// Output directory
				String chosenOutputDirectoryPath = tfDestination.getText();

				if (StringUtils.isNotEmpty(chosenOutputDirectoryPath)) {
					File chosenOutputDirectory = new File(chosenOutputDirectoryPath);
					if (chosenOutputDirectory.isDirectory()) {
						// Set label
						lStatus.setText(LABEL_PROCESSING);

						// Start number of points in label calculator
						dotsNumberToDisplayCalculatingWorker = new DotsNumberToDisplayCalculatingWorker();
						dotsNumberToDisplayCalculatingWorker.addPropertyChangeListener(new PropertyChangeListener() {

							@Override
							public void propertyChange(PropertyChangeEvent e) {
								processLabelOrProgressBar(e);
							}
						});
						dotsNumberToDisplayCalculatingWorker.execute();

						musicSorterWorker = new MusicSorterWorker(chosenInputDirectory, chosenOutputDirectory);
						musicSorterWorker.addPropertyChangeListener(new PropertyChangeListener() {

							@Override
							public void propertyChange(PropertyChangeEvent e) {
								processLabelOrProgressBar(e);
							}
						});

						musicSorterWorker.execute();
					} else {
						tfDestination.setBackground(Color.RED);
						processStopAction();
					}
				} else {
					tfDestination.setBackground(Color.RED);
					processStopAction();
				}
			} else {
				tfSource.setBackground(Color.RED);
				processStopAction();
			}
		} else {
			tfSource.setBackground(Color.RED);
			processStopAction();
		}
	}

	private void processLabelOrProgressBar(PropertyChangeEvent e) {

		String propertyName = e.getPropertyName();

		if (propertyName.equals(DotsNumberToDisplayCalculatingWorker.DOTS_NUMBER_TO_DISPLAY_PROPERTY)) {

			String statusLabelText = lStatus.getText().replace(".", "");

			if (statusLabelText.equals(LABEL_PROCESSING)) {
				Integer dotsNumber = (Integer) e.getNewValue();
				for (int i = 0; i < dotsNumber; i++) {
					statusLabelText += ".";
				}

				lStatus.setText(statusLabelText);
			}
		} else if (propertyName.equals(MusicSorterWorker.TOTAL_FOUND_FILES_NUMBER_PROPERTY)) {
			pbar.setIndeterminate(false);
			pbar.setMaximum((int) e.getNewValue());
		} else if (propertyName.equals(MusicSorterWorker.PROCESSED_FILES_NUMBER_PROPERTY)) {
			pbar.setValue((int) e.getNewValue());
		} else if (propertyName.equals(MusicSorterWorker.ERROR_LABEL_PROPERTY)) {
			processStopAction();
			lStatus.setText((String) e.getNewValue());
			lStatus.setForeground(Color.RED);
		} else if (propertyName.equals(MusicSorterWorker.ERROR_FILE_PROPERTY)) {
			textArea.append("\n" + (String) e.getNewValue() + "\n");
		} else if (propertyName.equals(MusicSorterWorker.DONE_LABEL_PROPERTY)) {
			processStopAction();
			lStatus.setText((String) e.getNewValue());
		}
	}

	protected void checkSourceAndDestinationDirectories() {

		if (!tfSource.getText().trim().isEmpty() && !tfDestination.getText().trim().isEmpty())
			bGo.setEnabled(true);
		else
			bGo.setEnabled(false);
	}

	public void updateDestTextField(DocumentEvent e) {

		if (cbSameAsSourceDirectory.isSelected())
			tfDestination.setText(tfSource.getText());
		;
	}

	class MyDocumentListener implements DocumentListener {

		public void insertUpdate(DocumentEvent e) {
			updateDestTextField(e);
		}

		public void removeUpdate(DocumentEvent e) {
			updateDestTextField(e);
		}

		public void changedUpdate(DocumentEvent e) {
			// Plain text components don't fire these events.
		}
	}

}
