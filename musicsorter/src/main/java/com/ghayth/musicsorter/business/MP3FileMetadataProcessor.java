package com.ghayth.musicsorter.business;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.exception.BusinessException;
import com.ghayth.musicsorter.exception.FileManagementException;
import com.ghayth.musicsorter.exception.TechnicalException;
import com.ghayth.musicsorter.helper.GeneralHelper;
import com.ghayth.musicsorter.helper.MP3MetadataHelper;
import com.ghayth.musicsorter.model.Song;
import com.ghayth.musicsorter.spotifyapi.SpotifyApiCaller;
import com.ghayth.musicsorter.spotifyapi.SpotifyApiDataParser;
import com.wrapper.spotify.model_objects.specification.Track;

/**
 * @author Ghayth AYARI
 *
 */
public class MP3FileMetadataProcessor {

	private final static Logger LOGGER = LoggerFactory.getLogger(MP3FileMetadataProcessor.class);

	SpotifyApiCaller spotifyApiCaller;
	FileManager fileManager;

	public MP3FileMetadataProcessor(File outputDirectory) throws TechnicalException {
		spotifyApiCaller = new SpotifyApiCaller();
		fileManager = new FileManager(outputDirectory.getAbsolutePath());
	}

	/**
	 * Processes song, and assumes it was already initialized.
	 * 
	 * @param filePath
	 * 
	 * @throws BusinessException
	 * @throws TechnicalException
	 * @throws FileManagementException
	 */
	public void process(String filePath) throws TechnicalException, BusinessException, FileManagementException {

		// Check that file is MP3
		String extension = FilenameUtils.getExtension(filePath);
		if (!StringUtils.equalsIgnoreCase(extension, "mp3")) {
			fileManager.moveFileToUnknownFilesDirectory(filePath, extension);
			return;
		}

		try {
			Song song = new Song(filePath);

			if (!song.isMissingMetadata()) {
				// Get Song meta-data from Spotify
				Track[] spotifyTracks = spotifyApiCaller.searchTrack(song.getArtist(), song.getTitle());

				if (ArrayUtils.isNotEmpty(spotifyTracks)) {
					SpotifyApiDataParser.setSongFieldsFromSpotifyApiTrack(song, spotifyTracks);
					MP3MetadataHelper.setSongMP3Metadata(song);

					// Check that file quality
					if (song.isBadQuality()) {
						fileManager.moveFileToBadQualityDirectory(song);
					} else {
						fileManager.moveFile(song);
					}
				} else if (song.isBadQuality()) {
					fileManager.moveFileToBadQualityDirectory(song);
				} else {
					// Search with artist and title in simple query
					String query = GeneralHelper.generateSearchQueryFromSongArtistAndTitle(song);
					spotifyTracks = spotifyApiCaller.searchTrack(query);

					if (ArrayUtils.isNotEmpty(spotifyTracks)) {
						SpotifyApiDataParser.setSongFieldsFromSpotifyApiTrack(song, spotifyTracks);
						MP3MetadataHelper.setSongMP3Metadata(song);
						fileManager.moveFileUncertainDirectory(song);
					} else {
						fileManager.moveFileToUnfoundOnSpotifyDirectory(song);
					}
				}

			} else {
				// Process case of missing metadata in song file
				// Search with filename
				String query = GeneralHelper.generateSearchQueryFromSongFileName(song);
				Track[] spotifyTracks = spotifyApiCaller.searchTrack(query);

				if (ArrayUtils.isNotEmpty(spotifyTracks)) {
					SpotifyApiDataParser.setSongFieldsFromSpotifyApiTrack(song, spotifyTracks);
					MP3MetadataHelper.setSongMP3Metadata(song);
					fileManager.moveFileUncertainDirectory(song);
				} else if (song.isBadQuality()) {
					fileManager.moveFileToBadQualityDirectory(song);
				} else {
					fileManager.moveFileToMissingMetadataDirectory(filePath);
				}
			}

		} catch (TechnicalException e) {
			LOGGER.error("A technical error occured while processing file " + filePath, e);
			fileManager.moveFileToErrorsDirectory(filePath);
			throw e;

		}
	}

}
