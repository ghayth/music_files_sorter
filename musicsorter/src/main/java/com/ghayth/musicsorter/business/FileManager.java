package com.ghayth.musicsorter.business;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.exception.FileManagementException;
import com.ghayth.musicsorter.exception.TechnicalException;
import com.ghayth.musicsorter.helper.GeneralHelper;
import com.ghayth.musicsorter.model.Song;
import com.ghayth.musicsorter.utils.DirectoryUtils;
import com.mpatric.mp3agic.NotSupportedException;

/**
 *
 */
public class FileManager {

	private final static Logger LOGGER = LoggerFactory.getLogger(FileManager.class);

	private static final String UNCERTAIN_DIRECTORY_NAME = "uncertain" + File.separator;
	private static final String UNFOUND_ON_SPOTIFY_DIRECTORY_NAME = "unfound_on_spotify" + File.separator;
	private static final String MISSING_METADATA_DIRECTORY_NAME = "missing_metadata" + File.separator;
	private static final String BAD_QUALITY_DIRECTORY_NAME = "bad_quality" + File.separator;
	private static final String PROBLEMS_DIRECTORY_NAME = "problems" + File.separator;
	private static final String UNKNOWN_FILES_DIRECTORY_NAME = "unknown_files" + File.separator;
	private static final String DOUBLE_FILES_DIRECTORY_NAME = "doubles" + File.separator;

	private String outputPath = null;

	public FileManager(String outputPath) {
		this.outputPath = DirectoryUtils.addSeparatorAtPathEndIfNecessary(outputPath);
	}

	/**
	 * Moves file to the subdirectory having the path=<path>\<year>. year is read
	 * from the song data.
	 * 
	 * @param file
	 * @param path
	 * @param date
	 * @throws TechnicalException
	 * 
	 * @throws Exception          if file cannot be moved
	 */
	public Boolean moveFile(Song song) throws FileManagementException, TechnicalException {

		// Create output directory
		String yearUri = DirectoryUtils.createUriBasedOnYear(song.getYear());
		String destinationPath = outputPath + yearUri;
		DirectoryUtils.createDirectoryIfNecessary(destinationPath);

		// Move file
		return moveFile(song, destinationPath);
	}

	public void moveFileToBadQualityDirectory(Song song) throws FileManagementException, TechnicalException {

		// Create output directory
		String badQualityDirectoryPath = outputPath + BAD_QUALITY_DIRECTORY_NAME
				+ DirectoryUtils.createUriBasedOnYear(song.getYear());
		DirectoryUtils.createDirectoryIfNecessary(badQualityDirectoryPath);

		// Move file
		moveFile(song, badQualityDirectoryPath);
	}

	public void moveFileToUnfoundOnSpotifyDirectory(Song song) throws FileManagementException, TechnicalException {

		// Create output directory
		String unfoundOnSpotifyDirectoryPath = outputPath + UNFOUND_ON_SPOTIFY_DIRECTORY_NAME
				+ DirectoryUtils.createUriBasedOnYear(song.getYear());
		DirectoryUtils.createDirectoryIfNecessary(unfoundOnSpotifyDirectoryPath);

		// Move file
		moveFile(song, unfoundOnSpotifyDirectoryPath);
	}

	public void moveFileToMissingMetadataDirectory(String filePath) throws FileManagementException {

		// Create output directory
		String missingMetadataDirectoryPath = outputPath + MISSING_METADATA_DIRECTORY_NAME;
		DirectoryUtils.createDirectoryIfNecessary(missingMetadataDirectoryPath);

		// Move file
		moveFile(new File(filePath), missingMetadataDirectoryPath);
	}

	public void moveFileUncertainDirectory(Song song) throws FileManagementException, TechnicalException {

		// Create output directory
		String uncertainDirectoryPath = outputPath + UNCERTAIN_DIRECTORY_NAME
				+ DirectoryUtils.createUriBasedOnYear(song.getYear());
		DirectoryUtils.createDirectoryIfNecessary(uncertainDirectoryPath);

		// Move file
		moveFile(song, uncertainDirectoryPath);
	}

	public void moveFileToErrorsDirectory(String filePath) throws FileManagementException {

		// Create output directory
		String exceptionsDirectoryPath = outputPath + PROBLEMS_DIRECTORY_NAME;
		DirectoryUtils.createDirectoryIfNecessary(exceptionsDirectoryPath);

		// Move file
		moveFile(new File(filePath), exceptionsDirectoryPath);
	}

	public void moveFileToUnknownFilesDirectory(String filePath, String extension) throws FileManagementException {

		// Create unknown files directory if needed
		String unknownFilesDirectoryPath = outputPath + UNKNOWN_FILES_DIRECTORY_NAME;
		DirectoryUtils.createDirectoryIfNecessary(unknownFilesDirectoryPath);

		// Create extension directory if needed
		String outputPathWithExtension = unknownFilesDirectoryPath + extension + File.separator;
		DirectoryUtils.createDirectoryIfNecessary(outputPathWithExtension);

		moveFile(new File(filePath), outputPathWithExtension);
	}

	private Boolean moveFile(Song song, String newPath) throws FileManagementException, TechnicalException {

		String newSongName = GeneralHelper.generateFileNameFromSong(song);

		String fileExtension = FilenameUtils.getExtension(song.getPath());

		String newSongNameWithExtension = newSongName + "." + fileExtension;

		// Check that file does not exist in the new destination
		String destinationPath = newPath + newSongNameWithExtension;
		File destinationFile = new File(destinationPath);

		// Check doubles (file already exists in destination directory)
		File doubledFile = checkDoublesAndUpdateDestinationFileNameIfNecessary(new File(destinationPath), newPath);
		if (doubledFile != null) {
			destinationFile = doubledFile;
		}

		// Move file
		try {
			song.getMp3File().save(destinationPath);

			// Delete original file
			File originalFile = new File(song.getPath());

			if (!originalFile.delete()) {
				throw new FileManagementException("Unable to delete file " + song.getPath());
			}
		} catch (NotSupportedException e) {
			throw new TechnicalException(e);
		} catch (Exception e) {
			String errorMessage = "Unable to copy file " + song.getPath() + " --> " + destinationFile.getAbsolutePath();
			throw new FileManagementException(errorMessage, e);
		}

		return true;
	}

	private Boolean moveFile(File fileToMove, String newPath) throws FileManagementException {

		// Check that file does not exist in the new destination
		String fileName = fileToMove.getName();
		String destinationPath = newPath + fileName;
		File destinationFile = new File(destinationPath);

		// Check doubles (file already exists in destination directory)
		File doubledFile = checkDoublesAndUpdateDestinationFileNameIfNecessary(destinationFile, newPath);
		if (doubledFile != null) {
			destinationFile = doubledFile;
		}

		// Move file
		if (!fileToMove.renameTo(destinationFile)) {
			String errorMessage = "Unable to move file " + fileToMove.getAbsolutePath() + " --> "
					+ destinationFile.getAbsolutePath();
			throw new FileManagementException(errorMessage);
		}

		return true;
	}

	/**
	 * Checks if file already exists. And if that's the case, another file is
	 * created as a double.
	 * 
	 * @param fileToCheck
	 * @param newPath
	 * @param fileName
	 * 
	 * @return
	 * 
	 * @throws FileManagementException
	 */
	private File checkDoublesAndUpdateDestinationFileNameIfNecessary(File fileToCheck, String newPath)
			throws FileManagementException {

		// Prepare return
		File resultFile = null;

		if (fileToCheck.exists()) {
			String fileName = FilenameUtils.getName(fileToCheck.getAbsolutePath());

			LOGGER.warn("File " + fileToCheck.getName() + " already exists in " + newPath
					+ "!! --> Moving to doubles  directory...");

			// Create doubles directory if needed
			String doublesDirectoryPath = outputPath + DOUBLE_FILES_DIRECTORY_NAME;
			DirectoryUtils.createDirectoryIfNecessary(doublesDirectoryPath);

			// Create new directory path in doubles directory
			String pathInDestinationAfterOutputPath = newPath.replace(outputPath, "");
			String newPathInDoublesDirectory = doublesDirectoryPath + pathInDestinationAfterOutputPath;
			DirectoryUtils.createDirectoryIfNecessary(newPathInDoublesDirectory);

			resultFile = new File(newPathInDoublesDirectory + fileToCheck.getName());

			// Rename file if it also exists in doubles directory
			String fileExtension = FilenameUtils.getExtension(fileName);
			String fileNameWithoutExtension = FilenameUtils.getBaseName(fileName);

			int i = 2;
			while (resultFile.exists()) {
				LOGGER.warn("File " + resultFile.getName() + " already exists in " + newPathInDoublesDirectory
						+ "!! --> Renaming...");

				// Rename file
				String newFileName = newPathInDoublesDirectory + fileNameWithoutExtension + "_" + String.valueOf(i)
						+ "." + fileExtension;

				resultFile = new File(newFileName);
				if (!resultFile.exists()) {
					break;
				}
				i++;
			}
		}

		return resultFile;
	}

}
