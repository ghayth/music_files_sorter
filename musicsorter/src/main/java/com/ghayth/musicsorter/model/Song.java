package com.ghayth.musicsorter.model;

import org.apache.commons.lang3.StringUtils;

import com.ghayth.musicsorter.exception.BusinessException;
import com.ghayth.musicsorter.exception.TechnicalException;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.Mp3File;

/**
 * @author Ghayth AYARI
 *
 */
public class Song {

	private String artist;
	private String title;
	private String year;
	private String album;
	private String albumArtist;
	private String trackNumber;
	private String url;
	private String imageURL;
	private String path;
	private Mp3File mp3File;

	private boolean missingMetadata = false;
	private boolean badQuality = false;

	public Song(String path) throws TechnicalException, BusinessException {
		this.path = path;

		// Open MP3 file
		try {
			mp3File = new Mp3File(path);
		} catch (Exception e) {
			throw new TechnicalException("Unable to open MP3 file!", e);
		}

		// Copy artist and title information from it
		String artist = null;
		String title = null;
		String year = null;

		if (mp3File.hasId3v1Tag()) {
			ID3v1 id3v1Tag = mp3File.getId3v1Tag();
			artist = id3v1Tag.getArtist();
			title = id3v1Tag.getTitle();
			year = id3v1Tag.getYear();
		}

		if (StringUtils.isBlank(artist) && mp3File.hasId3v2Tag()) {
			artist = mp3File.getId3v2Tag().getArtist();
		}

		if (StringUtils.isBlank(title) && mp3File.hasId3v2Tag()) {
			title = mp3File.getId3v2Tag().getTitle();
		}

		if (StringUtils.isBlank(year) && mp3File.hasId3v2Tag()) {
			year = mp3File.getId3v2Tag().getYear();
		}

		this.artist = StringUtils.replace(artist, "_", " ");
		this.title = StringUtils.replace(title, "_", " ");
		this.year = year;

		if (StringUtils.isEmpty(this.artist) || StringUtils.isEmpty(this.title)) {
			this.missingMetadata = true;
		}

		if (mp3File.getBitrate() < 320) {
			this.setBadQuality(true);
		}
	}

	/**
	 * @param artist
	 * @param title
	 * @param year
	 * @param album
	 * @param albumArtist
	 * @param trackNumber
	 * @param url
	 * @param imageURL
	 * @param path
	 */
	public Song(String artist, String title, String year, String album, String albumArtist, String trackNumber,
			String url, String imageURL, String path) {
		super();
		this.artist = artist;
		this.title = title;
		this.year = year;
		this.album = album;
		this.albumArtist = albumArtist;
		this.trackNumber = trackNumber;
		this.url = url;
		this.imageURL = imageURL;
		this.path = path;
	}

	@Override
	public String toString() {
		return "Song [artist=" + artist + ", title=" + title + ", year=" + year + ", album=" + album + ", albumArtist="
				+ albumArtist + ", trackNumber=" + trackNumber + ", url=" + url + ", imageURL=" + imageURL + ", path="
				+ path + "]";
	}

	/**
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}

	/**
	 * @return the albumArtist
	 */
	public String getAlbumArtist() {
		return albumArtist;
	}

	/**
	 * @param albumArtist the albumArtist to set
	 */
	public void setAlbumArtist(String albumArtist) {
		this.albumArtist = albumArtist;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the album
	 */
	public String getAlbum() {
		return album;
	}

	/**
	 * @param album the album to set
	 */
	public void setAlbum(String album) {
		this.album = album;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the trackNumber
	 */
	public String getTrackNumber() {
		return trackNumber;
	}

	/**
	 * @param trackNumber the trackNumber to set
	 */
	public void setTrackNumber(String trackNumber) {
		this.trackNumber = trackNumber;
	}

	/**
	 * @return the uRL
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the uRL to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the imageURL
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * @param imageURL the imageURL to set
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the mp3File
	 */
	public Mp3File getMp3File() {
		return mp3File;
	}

	/**
	 * @return the missingMetadata
	 */
	public boolean isMissingMetadata() {
		return missingMetadata;
	}

	/**
	 * @param missingMetadata the missingMetadata to set
	 */
	public void setMissingMetadata(boolean missingMetadata) {
		this.missingMetadata = missingMetadata;
	}

	/**
	 * @return the badQuality
	 */
	public boolean isBadQuality() {
		return badQuality;
	}

	/**
	 * @param badQuality the badQuality to set
	 */
	public void setBadQuality(boolean badQuality) {
		this.badQuality = badQuality;
	}

}
