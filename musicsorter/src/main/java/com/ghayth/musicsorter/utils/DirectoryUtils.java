package com.ghayth.musicsorter.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.exception.FileManagementException;

/**
 * @author Ghayth AYARI
 *
 */
public class DirectoryUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(DirectoryUtils.class);

	/**
	 * Adds a separator at the end of the provided path if that path does not
	 * contain one at the end.
	 * 
	 * @param path
	 */
	public static String addSeparatorAtPathEndIfNecessary(String path) {

		// Initialize result
		String result = path;

		if (!StringUtils.endsWith(path, File.separator)) {
			result += File.separator;
		}

		return result;
	}

	/**
	 * Creates a directory based on the provided directoryToCreate.
	 * 
	 * @param directoryPathToCreate
	 * 
	 * @throws Exception
	 */
	public static void createDirectoryIfNecessary(String directoryPathToCreate) throws FileManagementException {

		File directoryToCreate = new File(directoryPathToCreate);

		if (!directoryToCreate.exists()) {
			if (!directoryToCreate.mkdirs()) {
				String errorMessage = "Unable to create " + directoryToCreate.getAbsolutePath();
				LOGGER.error(errorMessage);
				throw new FileManagementException(errorMessage);
			}
		}
	}

	/**
	 * Gets a list of all files present in the provided directory, and all its
	 * sub-directories.
	 * 
	 * @param directory
	 * 
	 * @return
	 * 
	 * @throws Exception when provided directory is empty
	 */
	public static List<String> getAllFilesPaths(File directory) {

		// Check that input directory contains files
		File[] filesList = directory.listFiles();
		if (filesList == null) {
			return null;
		}

		// Prepare result
		List<String> foundFilesPaths = new ArrayList<String>();

		// Process files one by one
		for (File file : filesList) {
			if (file.isDirectory()) {
				LOGGER.info("Found directory: " + file.getAbsoluteFile());
				foundFilesPaths.addAll(getAllFilesPaths(file));
			} else {
				LOGGER.debug("Found file: " + file.getAbsoluteFile());
				foundFilesPaths.add(file.getAbsolutePath());
			}
		}

		return foundFilesPaths;
	}

	public static String createUriBasedOnYear(String year) {

		String result = "";

		if (StringUtils.isNotEmpty(year)) {
			if (year.length() == 4) {
				result = year.substring(0, 3) + "0s" + File.separator;
			}

			result += year + File.separator;
		}

		return result;
	}

}
