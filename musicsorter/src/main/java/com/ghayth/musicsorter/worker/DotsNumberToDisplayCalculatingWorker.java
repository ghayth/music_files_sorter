package com.ghayth.musicsorter.worker;

import javax.swing.SwingWorker;

/**
 * @author Ghayth AYARI
 *
 */
public class DotsNumberToDisplayCalculatingWorker extends SwingWorker<Void, Integer> {

	public static final String DOTS_NUMBER_TO_DISPLAY_PROPERTY = "dotsNumberToDisplay";

	private Integer increment = 0;

	@Override
	protected Void doInBackground() throws Exception {

		increment = 0;

		while (true) {
			Thread.sleep(500);
			Integer oldValue = increment;
			if (increment == 3) {
				increment = 0;
			}

			increment++;
			firePropertyChange(DOTS_NUMBER_TO_DISPLAY_PROPERTY, oldValue, increment);
		}
	}

}
