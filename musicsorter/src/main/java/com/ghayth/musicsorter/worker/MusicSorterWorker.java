package com.ghayth.musicsorter.worker;

import java.io.File;
import java.util.List;

import javax.swing.SwingWorker;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.business.MP3FileMetadataProcessor;
import com.ghayth.musicsorter.exception.BusinessException;
import com.ghayth.musicsorter.exception.TechnicalException;
import com.ghayth.musicsorter.utils.DirectoryUtils;

/**
 * @author Ghayth AYARI
 *
 */
public class MusicSorterWorker extends SwingWorker<Void, Void> {

	private final static Logger LOGGER = LoggerFactory.getLogger(MusicSorterWorker.class);

	public static final String TOTAL_FOUND_FILES_NUMBER_PROPERTY = "totalFoundFilesNumber";
	public static final String PROCESSED_FILES_NUMBER_PROPERTY = "processedFilesNumber";
	public static final String ERROR_LABEL_PROPERTY = "errorLabel";
	public static final String DONE_LABEL_PROPERTY = "doneLabel";
	public static final String ERROR_FILE_PROPERTY = "errorFile";

	private File inputDirectory;
	private File outputDirectory;

	public MusicSorterWorker(File inputDirectory, File outputDirectory) {
		this.inputDirectory = inputDirectory;
		this.outputDirectory = outputDirectory;
	}

	@Override
	protected Void doInBackground() {

		// Get list of files to be processed
		List<String> filesToProcessPaths = DirectoryUtils.getAllFilesPaths(inputDirectory);

		LOGGER.info("Found " + filesToProcessPaths.size() + " files in directory " + inputDirectory);

		if (CollectionUtils.isNotEmpty(filesToProcessPaths)) {
			firePropertyChange(TOTAL_FOUND_FILES_NUMBER_PROPERTY, 0, filesToProcessPaths.size());
		} else {
			firePropertyChange(ERROR_LABEL_PROPERTY, null, "No files found!");
			return null;
		}

		// Initialize MediaProcessor
		MP3FileMetadataProcessor mp3FileMetadataProcessor;
		try {
			mp3FileMetadataProcessor = new MP3FileMetadataProcessor(outputDirectory);
		} catch (TechnicalException e) {
			String errorMessage = "Unable to initialize processor!";
			LOGGER.error(errorMessage, e);
			firePropertyChange(ERROR_LABEL_PROPERTY, null, errorMessage + " - " + e.getMessage());
			return null;
		}

		// Initialize processing status and processed files number
		Boolean status = true;
		int processedFilesNumber = 0;

		// Process files one by one
		for (String filePath : filesToProcessPaths) {
			boolean currentFileProcessingStatus = true;

			try {
				mp3FileMetadataProcessor.process(filePath);
			} catch (TechnicalException e) {
				currentFileProcessingStatus = false;
				String errorMessage = "Technical error - " + filePath;
				firePropertyChange(ERROR_FILE_PROPERTY, null, e.getMessage() + " - " + errorMessage);
			} catch (BusinessException e) {
				// Do nothing
			} catch (Exception e) {
				currentFileProcessingStatus = false;
				String errorMessage = "Technical error - " + filePath;
				LOGGER.error(errorMessage, e);
				firePropertyChange(ERROR_FILE_PROPERTY, null, e.getMessage() + " - " + errorMessage);
			}

			status &= currentFileProcessingStatus;
			processedFilesNumber++;
			firePropertyChange(PROCESSED_FILES_NUMBER_PROPERTY, null, processedFilesNumber);

			if (isCancelled()) {
				LOGGER.warn("Worker received cancellation order!");
				break;
			}
		}

		if (status) {
			firePropertyChange(DONE_LABEL_PROPERTY, null, "Completed.");
		} else {
			firePropertyChange(ERROR_LABEL_PROPERTY, null, "Completed with errors!");
		}
		return null;
	}

}
