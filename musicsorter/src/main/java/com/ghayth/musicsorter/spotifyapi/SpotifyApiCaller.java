package com.ghayth.musicsorter.spotifyapi;

import java.io.IOException;

import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.exception.TechnicalException;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import com.wrapper.spotify.requests.data.search.simplified.SearchTracksRequest;

/**
 * Searches music using the Deezer API.
 * 
 * @author Ghayth AYARI
 *
 */
public class SpotifyApiCaller {

	private final static Logger LOGGER = LoggerFactory.getLogger(SpotifyApiCaller.class);

	private static final String CLIENT_ID = "356bc75c809042c386b1e46d42ca458c";
	private static final String CLIENT_SECRET = "19dfbfa087d9480bb9967f2d8eedc283";

	private static final SpotifyApi SPOTIFY_API = new SpotifyApi.Builder().setClientId(CLIENT_ID)
			.setClientSecret(CLIENT_SECRET).build();

	private static final ClientCredentialsRequest CLIENT_CREDENTIALS_REQUEST = SPOTIFY_API.clientCredentials().build();

	/**
	 * @throws TechnicalException
	 */
	public SpotifyApiCaller() throws TechnicalException {
		try {
			configureClientCredentials();
		} catch (ParseException | SpotifyWebApiException | IOException e) {
			throw new TechnicalException(e);
		}
	}

	/**
	 * Searches a track on the Spotify API
	 * 
	 * @param artist
	 * @param trackName
	 * 
	 * @return
	 * 
	 * @throws TechnicalException when a technical error occurs while processing the
	 *                            search request.
	 */
	public Track[] searchTrack(String artist, String trackName) throws TechnicalException {

		// Create query
		String query = "artist:" + artist + " track:" + trackName;

		return call(query);
	}

	/**
	 * @param query
	 * @param searchTracksRequest
	 * @return
	 * @throws TechnicalException
	 */
	private Track[] call(String query) throws TechnicalException {

		final SearchTracksRequest searchTracksRequest = SPOTIFY_API.searchTracks(query).build();

		try {
			Paging<Track> trackPaging = searchTracksRequest.execute();
			LOGGER.info("For query '" + query + "' found total: " + trackPaging.getTotal());

			return trackPaging.getItems();

		} catch (ParseException | SpotifyWebApiException | IOException e) {
			throw new TechnicalException(e);
		}
	}

	/**
	 * Searches a track on the Spotify API
	 * 
	 * @param artist
	 * @param trackName
	 * 
	 * @return
	 * 
	 * @throws TechnicalException when a technical error occurs while processing the
	 *                            search request.
	 */
	public Track[] searchTrack(String query) throws TechnicalException {

		return call(query);
	}

	public static void configureClientCredentials() throws ParseException, SpotifyWebApiException, IOException {
		final ClientCredentials clientCredentials = CLIENT_CREDENTIALS_REQUEST.execute();

		// Set access token for further "SPOTIFY_API" object usage
		SPOTIFY_API.setAccessToken(clientCredentials.getAccessToken());

		LOGGER.info("Spotify token expires in: " + clientCredentials.getExpiresIn() + " seconds");
	}

	public static void main(String[] args) {
		SpotifyApiCaller spotifyApiCaller;
		try {
			spotifyApiCaller = new SpotifyApiCaller();
			Track[] result = spotifyApiCaller.searchTrack("C.C.S. ", "whole lotta love");

			for (Track track : result) {
				System.out.println(track.getName());
				System.out.println(track.getArtists()[0].getName());
				System.out.println(track.getAlbum().getName());
				System.out.println(track.getAlbum().getReleaseDate());
			}
		} catch (TechnicalException e) {
			e.printStackTrace();
		}
	}

}
