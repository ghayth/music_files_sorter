package com.ghayth.musicsorter.spotifyapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghayth.musicsorter.exception.TechnicalException;
import com.ghayth.musicsorter.model.Song;
import com.wrapper.spotify.enums.ReleaseDatePrecision;
import com.wrapper.spotify.model_objects.specification.AlbumSimplified;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.ExternalUrl;
import com.wrapper.spotify.model_objects.specification.Image;
import com.wrapper.spotify.model_objects.specification.Track;

/**
 * @author Ghayth AYARI
 *
 */
public class SpotifyApiDataParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpotifyApiDataParser.class);

	private static final SimpleDateFormat YEAR_MONTH_DAY_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat YEAR_MONTH_DATE_FORMAT = new SimpleDateFormat("yyyy-MM");
	private static final SimpleDateFormat YEAR_DATE_FORMAT = new SimpleDateFormat("yyyy");

	/**
	 * Converts a Track object got from Spotify API to a Song object.
	 * 
	 * @param spotifyTrack
	 * 
	 * @return
	 * 
	 * @throws TechnicalException
	 */
	public static void setSongFieldsFromSpotifyApiTrack(Song song, Track[] spotifyTracks) throws TechnicalException {

		if (ArrayUtils.isNotEmpty(spotifyTracks)) {

			// Find the oldest track
			Track spotifyTrack = getTheOldestSpotifyTrack(spotifyTracks);

			// Set title
			if (StringUtils.isNotEmpty(spotifyTrack.getName())) {
				song.setTitle(spotifyTrack.getName());
			}

			// Set track number
			if (spotifyTrack.getTrackNumber() != null) {
				song.setTrackNumber(String.valueOf(spotifyTrack.getTrackNumber()));
			}

			// Extract artists
			setSongArtistsFromSpotifyApi(song, spotifyTrack);

			// Extract album information
			setSongFieldsFromSpotifyApiAlbum(song, spotifyTrack.getAlbum());

			// URL
			setSongURLFromSpotifyApiExternalUrls(song, spotifyTrack.getExternalUrls());
		}
	}

	protected static Track getTheOldestSpotifyTrack(Track[] spotifyTracks) throws TechnicalException {

		Track result = spotifyTracks[0];

		if (spotifyTracks.length > 1) {
			for (Track track : spotifyTracks) {
				if (track.getAlbum() != null && track.getAlbum().getName() != null
						&& !StringUtils.containsIgnoreCase(track.getAlbum().getName(), "karaoke")
						&& !StringUtils.containsIgnoreCase(track.getName(), "karaoke")) {
					String albumYear = extractYearFromSpotifyApiAlbumReleaseDate(track.getAlbum());
					String resultYear = extractYearFromSpotifyApiAlbumReleaseDate(result.getAlbum());

					if (Integer.valueOf(albumYear) < Integer.valueOf(resultYear)) {
						result = track;
					}
				}
			}
		}

		return result;
	}

	/**
	 * Sets song's URL from Spotify ExtrenalUrl entity when it is not empty.
	 * 
	 * @param song
	 * @param externalUrls
	 */
	protected static void setSongURLFromSpotifyApiExternalUrls(Song song, ExternalUrl externalUrls) {

		if (externalUrls != null) {
			Map<String, String> urlMap = externalUrls.getExternalUrls();
			if (MapUtils.isNotEmpty(urlMap)) {
				Set<String> keySet = urlMap.keySet();

				if (keySet.size() == 1) {
					String url = urlMap.get(keySet.iterator().next());
					if (StringUtils.isNotEmpty(url)) {
						song.setUrl(url);
					}
				} else {
					LOGGER.warn("Too many URLs found for " + song + ": " + keySet);
				}
			}
		}
	}

	/**
	 * Sets song's album name, year and image URL when they are not empty.
	 * Extraction is made from the Spotify API AlbumSimplified entity.
	 * 
	 * @param song
	 * @param albumFromSpotifyApi
	 * 
	 * @throws TechnicalException
	 */
	protected static void setSongFieldsFromSpotifyApiAlbum(Song song, AlbumSimplified albumFromSpotifyApi)
			throws TechnicalException {

		AlbumSimplified albumFromSpotify = albumFromSpotifyApi;
		if (albumFromSpotify != null) {

			// Set album name
			if (StringUtils.isNotEmpty(albumFromSpotify.getName())) {
				song.setAlbum(albumFromSpotify.getName());
			}

			// Set album artists
			String albumArtist = getTrackArtistFromSpotifyData(albumFromSpotify.getArtists());
			if (StringUtils.isNotEmpty(albumArtist)) {
				song.setAlbumArtist(albumArtist);
			}

			// Set song year
			song.setYear(extractYearFromSpotifyApiAlbumReleaseDate(albumFromSpotify));

			// Set album image
			Image[] albumImages = albumFromSpotify.getImages();
			if (ArrayUtils.isNotEmpty(albumImages)) {
				for (Image image : albumImages) {
					if (image.getWidth() >= 300 && image.getWidth() <= 600 && StringUtils.isNotEmpty(image.getUrl())) {
						song.setImageURL(image.getUrl());
						break;
					}
				}
			}
		}
	}

	/**
	 * Gets the song's year from the Spotify API AlbumSimplified entity.
	 * 
	 * @param albumFromSpotify
	 * 
	 * @return
	 * 
	 * @throws TechnicalException when an error occurs while trying to extract the
	 *                            year.
	 */
	protected static String extractYearFromSpotifyApiAlbumReleaseDate(AlbumSimplified albumFromSpotify)
			throws TechnicalException {

		ReleaseDatePrecision releaseDatePrecision = albumFromSpotify.getReleaseDatePrecision();

		// Extract date from album
		Date albumReleaseDate = null;
		try {
			if (releaseDatePrecision.equals(ReleaseDatePrecision.DAY)) {
				albumReleaseDate = YEAR_MONTH_DAY_DATE_FORMAT.parse(albumFromSpotify.getReleaseDate());

			} else if (releaseDatePrecision.equals(ReleaseDatePrecision.MONTH)) {
				albumReleaseDate = YEAR_MONTH_DATE_FORMAT.parse(albumFromSpotify.getReleaseDate());
			} else if (releaseDatePrecision.equals(ReleaseDatePrecision.YEAR)) {
				albumReleaseDate = YEAR_DATE_FORMAT.parse(albumFromSpotify.getReleaseDate());
			} else {
				throw new TechnicalException("Unexpected release date type from Spotify API: " + releaseDatePrecision);
			}
		} catch (ParseException e) {
			throw new TechnicalException("Unable to parse date " + albumFromSpotify.getReleaseDate(), e);
		}

		// Extract the year from the date
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(albumReleaseDate);

		return String.valueOf(calendar.get(Calendar.YEAR));
	}

	/**
	 * Sets song artists from Spotify API data.
	 * 
	 * @param song
	 * @param track
	 */
	protected static void setSongArtistsFromSpotifyApi(Song song, Track track) {

		String artist = getTrackArtistFromSpotifyData(track.getArtists());
		if (StringUtils.isNotEmpty(artist)) {
			song.setArtist(artist);
		}
	}

	protected static String getTrackArtistFromSpotifyData(ArtistSimplified[] artists) {

		String artist = null;
		if (ArrayUtils.isNotEmpty(artists)) {
			for (ArtistSimplified artistFromSpotify : artists) {
				if (artist == null) {
					artist = artistFromSpotify.getName();
				} else {
					artist += " & " + artistFromSpotify.getName();
				}
			}
		}
		return artist;
	}

}
